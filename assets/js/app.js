//@prepros-prepend ../../node_modules/jquery/dist/jquery.js
//@prepros-prepend ../../node_modules/jquery-youtube-background/src/jquery.youtubebackground.js
//@prepros-prepend ../../node_modules/jquery-countdown/dist/jquery.countdown.js
//@prepros-prepend ../../node_modules/sweetalert2/dist/sweetalert2.js
//@prepros-prepend ../../node_modules/lightgallery/dist/js/lightgallery.min.js
//@prepros-prepend ../../node_modules/lightgallery/modules/lg-video.js

//@prepros-prepend ../../node_modules/rellax/rellax.js/


$(document).ready(function() {
	$('#video').YTPlayer({
	    fitToBackground: true,
	    videoId: '3ze8lljiVMk',
			repeat:true
	});


	$('#clock').countdown('2019/08/14', function(event) {
	  var $this = $(this).html(event.strftime(''
	    + '<span>%w</span> semaines '
	    + '<span>%d</span> jours '
	    + '<span>%H</span> h '
	    + '<span>%M</span> min '
	    + '<span>%S</span> sec'));
	});

	$('.mail').click(function(){
		swal({
		  title: 'Entrer votre adresse email',
		  input: 'email',
			position:'center',
			showCloseButton:'true'
		}).then(function (email) {
		  swal({
		    type: 'success',
		    html: 'Merci de votre inscription ',
				showCloseButton:'true'
		  })
		})
	});

  $("#lightgallery").lightGallery({
			mode:'lg-slide-vertical',
			hideBarsDelay:'999999'
		});

		var rellax = new Rellax('.rellax', {
    speed: -2,
    center: false,
    wrapper: null,
    round: true,
    vertical: true,
    horizontal: false
  });

});
