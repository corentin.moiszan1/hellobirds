<div id="gallery">
  <h2>Artistes</h2>
  <div id="lightgallery" class="row">
      <a class="nom col-lg-4 col-sm-5 col-xs-12" data-src="img/artistes/chassol.jpg">
          <img src="img/artistes/chassol.jpg" />
      </a>
      <a class="col-lg-4 col-sm-5 col-xs-12" data-src="img/artistes/kiddy_smile.jpg">
          <img src="img/artistes/kiddy_smile.jpg" />
      </a>
      <a class="col-lg-4 col-sm-5 col-xs-12" data-src="img/artistes/lesneu.jpg">
          <img src="img/artistes/lesneu.jpg" />
      </a>
      <a class="col-lg-4 col-sm-5 col-xs-12" data-src="img/artistes/the_hop.jpg">
          <img src="img/artistes/the_hop.jpg" />
      </a>
      <a class="col-lg-4 col-sm-5 col-xs-12" data-src="img/artistes/thris_tian.jpg">
          <img src="img/artistes/thris_tian.jpg" />
      </a>
      <a class="col-lg-4 col-sm-5 col-xs-12" data-src="img/artistes/hybu.jpg">
          <img src="img/artistes/hybu.jpg" />
      </a>
      <a class="col-lg-4 col-sm-5 col-xs-12" data-src="img/artistes/comala.png">
          <img src="img/artistes/comala.png" />
      </a>
      <a class="col-lg-4 col-sm-5 col-xs-12" data-src="img/artistes/mmpp.jpg">
          <img src="img/artistes/mmpp.jpg" />
      </a>
      <a class="col-lg-4 col-sm-5 col-xs-12" data-src="img/artistes/tash_lc.jpg">
          <img src="img/artistes/tash_lc.jpg" />
      </a>

  </div>
</div>
