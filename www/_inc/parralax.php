<div id='parallax'>
	<div class='grid-parallax'>
		<h1>Une nouvelle escapade seapop, cette fois-ci en terre bretonne!</h1>
		<h3>Des falaises d’Etretat à la côte d’Emeraude. Hello Birds prolonge l’été, les pieds dans l’eau, à Saint-Malo !</h3>
		<p>Ouvert, transversal et déambulatoire, Hello Birds est une invitation à découvrir le littoral au gré d’expériences musicales, sportives, gourmandes & hédonistes.</p>
	</div>
	<img class="rellax" data-rellax-speed="-1.7" src='img/parralax/parralax1.png' alt='Saint-Malo' id='photo1'/>
	<img class="rellax" data-rellax-speed="3" src='img/parralax/parralax2.png' alt='Hello' id='photo2'/>
	<img src='img/parralax/parralax3.png' alt='Birds' id='photo3'/>
</div>
