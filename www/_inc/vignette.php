<div id="vignette">
  <h2>Nature</h2>
  <h3>Découvrez la cité corsaire, accompagnés de nos guides et coachs de prestige.</h3>
  <div>
    <img class="col-lg-3 col-sm-12" src="img/vignette/run.png">
    <img class="col-lg-3 col-sm-12" src="img/vignette/balade.png">
    <img class="col-lg-3 col-sm-12" src="img/vignette/art.png">
  </div>
</div>
