<div id='parallax1'>
	<img class="rellax" data-rellax-speed="-1.7" src='img/parralax1/parralax1.png' alt='Huitre' id='photo1'/>
	<img class="rellax" data-rellax-speed="3" src='img/parralax1/parralax2.png' alt='Crepe' id='photo2'/>
	<img src='img/parralax1/parralax3.png' alt='Mains' id='photo3'/>
	<div class='grid-parallax1'>
		<h1>THE WORLD IS YOUR OYSTER.</h1>
		<h3>Toute la journée, retrouvez-nous pour déguster le meilleur des saveurs bretonnes: brunch, galettes revisitées, huîtres de Cancale, cidre artisanal, bière craft et autres spécialitées bretonnes, tout est là 👌</h3>
		<p>Hello Birds s’associe au réseau de restaurateurs solidaires <a href='http://hello-ernest.com/fr/' target='_blank'>Ernest</a> pour vous faire découvrir les meilleures saveurs bretonnes en collaboration avec les producteurs de la région.</p>
	</div>
	
</div>
