    <footer>
        <h2>Hello Birds,<br>It's a state of mind</h2>
      <ul id="lien">
        <li class="mail"><a>S'inscrire à la Newsletter</a></li>
        <li><a href="https://etretat.hellobirdsfestival.fr/">Hello Birds Etretat</a></li>
      </ul>
      <ul id="social">
        <li><a href="https://www.facebook.com/hellobirds/" target="_blank"><i class="fab fa-facebook fa-4x"></i></a></li>
        <li><a href="https://twitter.com/hellobirdsfest" target="_blank"><i class="fab fa-twitter fa-4x"></i></a></li>
        <li><a href="https://www.instagram.com/hello__birds/" target="_blank"><i class="fab fa-instagram fa-4x"></i></a></li>
      </ul>
      <div class='copyright'>
        <span>
          <p>Oct 2018 • © S.Lavollée & C.Moiszan • <a href='https://saintmalo.hellobirdsfestival.fr/' target='_blank'>Images : HBF Production</a> • <a href='https://www.youtube.com/watch?v=3ze8lljiVMk' target='_blank'>Vidéo : art de plage, YouTube</a></p>
        </span>
      </div>
    </footer>
    <script src="script.js"></script>
  </body>
</html>
